/**
 * Main application file
 */

'use strict';
var PORT = 3000;

var SET_TXT_URL = 'http://www.powerball.com/powerball/winnums-text.txt';
var LATEST_JACKPOT_URL = 'http://www.powerball.com/pb_home.asp';
var express = require('express');
var request = require('request');
var cheerio = require('cheerio');

// Setup server
var app = express();
var server = require('http').createServer(app);

/**
* get the latest set for the app
*/
app.get('/api/latest-set', function(req, res) {
  request.get({url: SET_TXT_URL}, function(err, httpResponse, body){
    if (err) { return res.json({err: err}); }

    //extract the first row from the text
    var rows = body.split("\n");
    //get the latest row
    if (rows.length < 2) {
      return res.json(null);
    }

    var row = rows[1];
    var data = row.split("  ");

    //TODO - should check data length
    return res.json({
      drawDate: data[0],
      WB1: data[1],
      WB2: data[2],
      WB3: data[3],
      WB4: data[4],
      WB5: data[4],
      PB: data[6],
      PP: data[7].trim()
    });
  });
});

app.get('/api/latest-jackpot', function(req, res) {
  request.get({url: LATEST_JACKPOT_URL}, function(err, httpResponse, body){
    if (err) { return res.json({err: err}); }

    var $ = cheerio.load(body, {
      normalizeWhitespace: true,
      xmlMode: true
    });
    var content = $('.content table').eq(2);
    if (!content) { return res.json({message: 'Non content found!'}); }
    var text = content.find('tr').eq(2).find('td').eq(2).text();

    //cast string to number for calculating
    var numbs = text.match(/\d/g);
    var number = Number(numbs.join(''));

    return res.json({
      cents: number * 100,
      usd: number
    });
  });
});

// Start server
server.listen(PORT, null, function () {
  console.log('Server listening on %d', PORT);
});

// Expose app
exports = module.exports = app;